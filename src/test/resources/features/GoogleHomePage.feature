
Feature: Verify the Google homepage functionality

  Scenario: Verify the search field on the google homepage
    Given a user launches the google home page
    When the google homepage is opened
    Then the search field is present on the homepage

  Scenario: Verify Auto Populate options are displayed on search
    Given a user is on google home page
    When the user input Automation for search
    Then the Auto Populate list gets displayed with keyword Automation
    And has the 'Google search' and 'I'm Feeling Lucky' buttons

  Scenario: Verify default links on header and footer for google homepage
    Given a user launches the google home page
    When the google homepage is opened
    Then all the links are displayed on the header
    And all the links are present on the footer

  Scenario: Verify the default country is set correctly on google home page
    Given a user launches the google home page
    When the google homepage is opened
    Then the default country is set correctly

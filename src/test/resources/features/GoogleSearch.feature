
Feature: Verify Google search functionality

  Scenario Outline: Verify Google search list displays for the searched keywords
    Given a user is on google home page
    When the user search for <searchText>
    Then all the search results are relevant to <searchText>
    Examples:
    |searchText|
    |Automation|
    |1234      |

   Scenario Outline: Verify AutoCorrect functionality for google Search
     Given a user is on google home page
     When the user search for <wrongText>
     Then the google auto corrects the <wrongText> to <autoCorrectText>
     And all the search results are relevant to <autoCorrectText>
     Examples:
     |wrongText|autoCorrectText|
     |autmation|automation     |
     |dowload  |download       |

  Scenario: Verify Google search checks for alternate options
    Given a user is on google home page
    When the user search for java8
    Then the google search checks for alternate option java 8






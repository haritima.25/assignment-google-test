package com.google.pagesobjects;

import com.google.TestContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class SearchResultPage {
    private final WebDriver driver;
    private final WebDriverWait wait;

    public SearchResultPage() {
        TestContext context = TestContext.getInstance();
        this.driver = context.getWebDriver();
        PageFactory.initElements(this.driver, this);
        this.wait = new WebDriverWait(this.driver, 30);
    }

    // Elements
    @FindBy(className = "aCOpRe")
    List<WebElement> searchDescription;

    @FindBy(id = "fprs")
    WebElement showingResultFor;

    @FindBy(id = "taw")
    WebElement didYouMeanText;



    // Actions

    public List<String> getSearchResultsDescriptions() {
        List<String> descriptionList = new ArrayList<>();
        searchDescription.stream().forEach(element -> descriptionList.add(element.getText()));
        return descriptionList;
    }

    public String getSearchResultForText() {
        return showingResultFor.getText();
    }

    public String getDidYouMeanText() {
        return didYouMeanText.getText();
    }

    public void waitForSearchPageLoad() {
        wait.until(ExpectedConditions.visibilityOf(didYouMeanText));
    }
}

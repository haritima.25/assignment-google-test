package com.google.pagesobjects;

import com.google.TestContext;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.stream.Collectors;

public class HomePage {
    private final WebDriver driver;
    private final WebDriverWait wait;

    public HomePage() {
        TestContext context = TestContext.getInstance();
        this.driver = context.getWebDriver();
        PageFactory.initElements(this.driver, this);
        this.wait = new WebDriverWait(this.driver, 30);
    }

    // Elements
    @FindBy(xpath = "//input[@class='gLFyf gsfi']")
    WebElement searchInput;

    @FindBy(className = "lnXdpd")
    WebElement googleLogo;

    @FindBy(xpath = "//span[text()='I agree']")
    WebElement agreeButton;

    @FindBy(className = "erkvQe")
    WebElement searchListPanel;

    @FindBy(xpath = "//li[@class='sbct'][@jsaction='click:.CLIENT;mouseover:.CLIENT']")
    List<WebElement> searchList;

    @FindBy(xpath = "//div[@class='aajZCb']//input[@class='gNO89b']")
    WebElement googleSearchButton;

    @FindBy(className = "RNmpXc")
    WebElement iAmFeelingLuckyButton;

    @FindBy(className = "gb_Xe")
    WebElement appsIcon;

    @FindBy(xpath = "//a[text() = 'About']")
    WebElement aboutLink;

    @FindBy(xpath = "//a[text() = 'Store']")
    WebElement storeLink;

    @FindBy(xpath = "//a[text() = 'Gmail']")
    WebElement gmailLink;

    @FindBy(xpath = "//a[text() = 'Images']")
    WebElement imagesLink;

    @FindBy(xpath = "//a[text() = 'Sign in']")
    WebElement signInLink;

    @FindBy(xpath = "//a[text() = 'Advertising']")
    WebElement advertisingLink;

    @FindBy(xpath = "//a[text() = 'Business']")
    WebElement businessLink;

    @FindBy(xpath = "//a[text()='  How Search works ']")
    WebElement howSearchWorksLink;

    @FindBy(className = "ktLKi")
    WebElement carbonNeutralLink;

    @FindBy(xpath = "//a[text() = 'Privacy']")
    WebElement privacyLink;

    @FindBy(xpath = "//a[text()= 'Terms']")
    WebElement termsLink;

    @FindBy(id = "Mses6b")
    WebElement settingsLink;

    @FindBy(className = "uU7dJb")
    WebElement country;

    @FindBy(xpath = "//button[text()[contains(.,'switch')]]")
    WebElement dontSwitch;


    // Actions
    public void getUrl(String url) {
        driver.get(url);
    }

    public void waitForGoogleLogo() {
        wait.until(ExpectedConditions.visibilityOf(googleLogo));
    }

    public void inputSearchValue(String inputValue) {
        searchInput.sendKeys(inputValue);
    }

    public void clearSearchInput() {
        searchInput.clear();
    }

    public List<String> getAutoPopulateSearchText() {
        return searchList.stream().map(e -> e.getText()).collect(Collectors.toList());
    }

    public boolean waitForAutoPopulateList() {
        return wait.until(ExpectedConditions.visibilityOf(searchListPanel)).isDisplayed();
    }

    public String getCountryText() {
        return country.getText();
    }

    public void acceptAllPopups() {
        List<WebElement> frames = driver.findElements(By.tagName("iframe"));
        if(frames.size() == 2){
            driver.switchTo().frame(1);
            agreeButton.click();
            driver.switchTo().frame(0);
            dontSwitch.click();
        } else if(frames.size() == 1) {
            if(frames.get(0).getAttribute("src").contains("consent")) {
                driver.switchTo().frame(0);
                agreeButton.click();
            }
            else if(frames.get(0).getAttribute("src").contains("widget")) {
                driver.switchTo().frame(0);
                dontSwitch.click();
            }
        }
        driver.switchTo().defaultContent();
    }

    public void clickGoogleSearchButton(){
        googleSearchButton.click();
    }

    public void waitForGoogleSearchButton() {
        wait.until(ExpectedConditions.visibilityOf(googleSearchButton));
    }

    // Checkers
    public boolean isSearchInputPresent() {
        return searchInput.isDisplayed();
    }

    public boolean isPopupPresent() {
        return driver.findElements(By.tagName("iframe")).size() > 0;
    }

    public boolean isGoogleSearchButtonDisplayed() {
        return googleSearchButton.isDisplayed();
    }

    public boolean isIAmLuckyButtonDisplayed() {
        return iAmFeelingLuckyButton.isDisplayed();
    }

    public boolean isSignInButtonDisplayed() {
        return signInLink.isDisplayed();
    }

    public boolean isAppsIconDisplayed() {
        return appsIcon.isDisplayed();
    }

    public boolean isImageSearchLinkDisplayed() {
        return imagesLink.isDisplayed();
    }

    public boolean isGmailLinkDisplayed() {
        return gmailLink.isDisplayed();
    }

    public boolean isStoreLinkDisplayed() {
        return storeLink.isDisplayed();
    }

    public boolean isAboutLinkDisplayed() {
        return aboutLink.isDisplayed();
    }

    public boolean isAdvertisingLinkDisplayed() {
        return advertisingLink.isDisplayed();
    }

    public boolean isBusinessLinkDisplayed() {
        return businessLink.isDisplayed();
    }

    public boolean isCarbonNeutralLinkDisplayed() {
        return carbonNeutralLink.isDisplayed();
    }

    public boolean isSettingsLinkDisplayed() {
        return settingsLink.isDisplayed();
    }

    public boolean isTermsLinkDisplayed() {
        return termsLink.isDisplayed();
    }

    public boolean isImagePrivacyLinkDisplayed() {
        return privacyLink.isDisplayed();
    }

    public boolean isHowSearchWorksLinkDisplayed() {
        return howSearchWorksLink.isDisplayed();
    }

}

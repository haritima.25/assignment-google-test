package com.google.stepdefinition;

import com.google.helpers.constants.GoogleConstants;
import com.google.steplibrary.HomePageLibrary;
import com.google.steplibrary.SearchPageLibrary;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class GoogleSearchSteps {
    SearchPageLibrary searchPageLibrary = new SearchPageLibrary();
    HomePageLibrary homePageLibrary = new HomePageLibrary();


    @Then("^all the search results are relevant to (.*)$")
    public void allTheSearchResultsAreRelevantToSearchText(String searchedText) {
        Assert.assertTrue(searchPageLibrary.isAllTheSearchRelevant(searchedText));
    }

    @When("^the user search for (.*)$")
    public void theUserSearchForSearchText(String searchedText) {
        homePageLibrary.inputSearchValue(searchedText);
        homePageLibrary.waitForGoogleSearchButton();
        homePageLibrary.selectGoogleSearch();
    }

    @Then("^the google search checks for alternate option (.*)$")
    public void theGoogleSearchChecksForAlternateOptions(String alternateText) {
        Assert.assertEquals(searchPageLibrary.getDidYouMeanText(), GoogleConstants.DID_YOU_MEAN_TEXT + alternateText);

    }

    @Then("^the google auto corrects the (.*) to (.*)$")
    public void theGoogleAutoCorrectsTheWrongTextToAutoCorrectText(String wrongText, String autoCorrectText) {
        Assert.assertEquals(searchPageLibrary.getSearchAutocorrectText(), GoogleConstants.SHOWING_RESULTS_FOR + autoCorrectText + GoogleConstants.INSTEAD_OF_TEXT + wrongText);
    }
}

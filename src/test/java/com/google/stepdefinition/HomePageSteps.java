package com.google.stepdefinition;

import com.google.TestContext;
import com.google.helpers.constants.GoogleConstants;
import com.google.helpers.constants.uiHelpers;
import com.google.steplibrary.HomePageLibrary;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertTrue;

public class HomePageSteps {
    private static final Logger LOGGER = LoggerFactory.getLogger(HomePageSteps.class);
    HomePageLibrary homePageLibrary = new HomePageLibrary();

    @Given("^a user launches the google home page$")
    public void aUserLaunchesTheGoogleHomePage() {
        homePageLibrary.getGoogleHomePage();
    }

    @When("^the google homepage is opened$")
    public void theGoogleHomepageIsOpened() {
        homePageLibrary.waitForHomePage();
        homePageLibrary.acceptPopup();
    }

    @Then("^the search field is present on the homepage$")
    public void theSearchFieldIsPresentOnTheHomepage() {
        Assert.assertTrue(homePageLibrary.isSearchInputFieldPresent());
    }

    @Given("^a user is on google home page$")
    public void aUserIsOnGoogleHomePage() {
        homePageLibrary.getGoogleHomePage();
        homePageLibrary.waitForHomePage();
        homePageLibrary.acceptPopup();
    }

    @When("^the user input (.*) for search$")
    public void aUserInputAutomationForSearch(String searchKeyword) {
        homePageLibrary.inputSearchValue(searchKeyword);
    }

    @Then("^the Auto Populate list gets displayed with keyword (.*)$")
    public void theAutoPopulateListGetsDisplayedWithKeywordAutomation(String searchKeyword) {
        assertTrue(homePageLibrary.isAutoPopulateListDisplayed());
        assertTrue(homePageLibrary.doesListContainSearchKeyword(searchKeyword));

    }

    @And("^has the 'Google search' and 'I'm Feeling Lucky' buttons$")
    public void hasTheGoogleSearchAndIMFeelingLuckyButtons() {
        Assert.assertTrue(homePageLibrary.isGoogleSearchDisplayed());
        Assert.assertTrue(homePageLibrary.isIAmFeelingLuckyButtonDisplayed());
    }

    @Then("^all the links are displayed on the header$")
    public void allTheLinksAreDisplayedOnTheHeader() {
        Assert.assertTrue(homePageLibrary.isAboutLinkDisplayed());
        Assert.assertTrue(homePageLibrary.isStoreLinkDisplayed());
        Assert.assertTrue(homePageLibrary.isGmailLinkDisplayed());
        Assert.assertTrue(homePageLibrary.isImageSearchLinkDisplayed());
        Assert.assertTrue(homePageLibrary.isAppsIconDisplayed());
        Assert.assertTrue(homePageLibrary.isSignInButtonDisplayed());
    }

    @And("^all the links are present on the footer$")
    public void allTheLinksArePresentOnTheFooter() {
        Assert.assertTrue(homePageLibrary.isAdvertisingLinkDisplayed());
        Assert.assertTrue(homePageLibrary.isStoreBusinessLinkDisplayed());
        Assert.assertTrue(homePageLibrary.isHowSearchWorksLinkDisplayed());
        Assert.assertTrue(homePageLibrary.isImagePrivacyLinkDisplayed());
        Assert.assertTrue(homePageLibrary.isAppsTermsLinkDisplayed());
        Assert.assertTrue(homePageLibrary.isSettingsLinkDisplayed());
        Assert.assertTrue(homePageLibrary.isCarbonNeutralLinkDisplayed());
    }

    @Then("^the default country is set correctly$")
    public void theDefaultCountryIsSetCorrectly() {
        Assert.assertEquals(homePageLibrary.getSetCountry(), uiHelpers.getCurrentLocation());
    }

    @After
    public void afterClass(Scenario scenario) {
        if (scenario.isFailed()) {
            File scrFile = ((TakesScreenshot)TestContext.getInstance().getWebDriver()).getScreenshotAs(OutputType.FILE);
            String path = GoogleConstants.FILE_PATH +scenario.getName()+".png";
            try {
                FileUtils.copyFile(scrFile, new File(path));
                LOGGER.info("***Placed screen shot in "+path+" ***");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        TestContext.getInstance().closeAll();
    }
}

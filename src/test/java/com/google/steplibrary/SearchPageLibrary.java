package com.google.steplibrary;

import com.google.pagesobjects.SearchResultPage;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class SearchPageLibrary {
    SearchResultPage searchResultPage = new SearchResultPage();

    public SearchPageLibrary() {
    }

    public boolean isAllTheSearchRelevant(String searchedText) {
        List<String> descriptionText = searchResultPage.getSearchResultsDescriptions();
        return descriptionText.stream().allMatch(text -> StringUtils.containsIgnoreCase(text,searchedText) || text.equals(StringUtils.EMPTY));
    }

    public String getSearchAutocorrectText() {
        return searchResultPage.getSearchResultForText();
    }

    public String getDidYouMeanText() {
        searchResultPage.waitForSearchPageLoad();
        return searchResultPage.getDidYouMeanText();
    }
}

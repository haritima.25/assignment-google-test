package com.google.steplibrary;

import com.google.helpers.constants.GoogleConstants;
import com.google.pagesobjects.HomePage;
import org.apache.commons.lang3.StringUtils;

public class HomePageLibrary {
    HomePage homePage = new HomePage();

    public void getGoogleHomePage() {
        homePage.getUrl(GoogleConstants.GOOGLE_URL);
    }

    public void waitForHomePage() {
        homePage.waitForGoogleLogo();
    }

    public boolean isSearchInputFieldPresent() {
        return homePage.isSearchInputPresent();
    }

    public void acceptPopup() {
        if (homePage.isPopupPresent()) {
            homePage.acceptAllPopups();
        }
    }


    public void inputSearchValue(String searchKeyword) {
        homePage.clearSearchInput();
        homePage.inputSearchValue(searchKeyword);
    }

    public boolean isAutoPopulateListDisplayed() {
        return homePage.waitForAutoPopulateList();
    }

    public boolean doesListContainSearchKeyword(String searchKeyword) {
        return homePage.getAutoPopulateSearchText().stream().allMatch(e -> StringUtils.containsIgnoreCase(e, searchKeyword));
    }

    public boolean isGoogleSearchDisplayed() {
        return homePage.isGoogleSearchButtonDisplayed();
    }

    public boolean isIAmFeelingLuckyButtonDisplayed() {
        return homePage.isIAmLuckyButtonDisplayed();
    }

    public boolean isAboutLinkDisplayed() {
        return homePage.isAboutLinkDisplayed();
    }

    public boolean isStoreLinkDisplayed() {
        return homePage.isStoreLinkDisplayed();
    }

    public boolean isGmailLinkDisplayed() {
        return homePage.isGmailLinkDisplayed();
    }

    public boolean isImageSearchLinkDisplayed() {
        return homePage.isImageSearchLinkDisplayed();
    }

    public boolean isAppsIconDisplayed() {
        return homePage.isAppsIconDisplayed();
    }

    public boolean isSignInButtonDisplayed() {
        return homePage.isSignInButtonDisplayed();
    }

    public boolean isAdvertisingLinkDisplayed() {
        return homePage.isAdvertisingLinkDisplayed();
    }

    public boolean isStoreBusinessLinkDisplayed() {
        return homePage.isBusinessLinkDisplayed();
    }

    public boolean isHowSearchWorksLinkDisplayed() {
        return homePage.isHowSearchWorksLinkDisplayed();
    }

    public boolean isImagePrivacyLinkDisplayed() {
        return homePage.isImagePrivacyLinkDisplayed();
    }

    public boolean isAppsTermsLinkDisplayed() {
        return homePage.isTermsLinkDisplayed();
    }

    public boolean isSettingsLinkDisplayed() {
        return homePage.isSettingsLinkDisplayed();
    }

    public boolean isCarbonNeutralLinkDisplayed() {
        return homePage.isCarbonNeutralLinkDisplayed();
    }

    public String getSetCountry() {
        return homePage.getCountryText();
    }

    public void waitForGoogleSearchButton() {
        homePage.waitForGoogleSearchButton();
    }

    public void selectGoogleSearch() {
        homePage.clickGoogleSearchButton();
    }
}

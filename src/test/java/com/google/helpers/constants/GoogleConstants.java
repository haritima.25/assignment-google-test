package com.google.helpers.constants;

public class GoogleConstants {

    //URL
    public static final String GOOGLE_URL = "https://www.google.com";


    //Test Constants
    public static final String SHOWING_RESULTS_FOR = "Showing results for ";
    public static final String INSTEAD_OF_TEXT = "\nSearch instead for ";
    public static final String DID_YOU_MEAN_TEXT = "Did you mean: ";

    //ScreenshotPath
    public static final String FILE_PATH = "target/Screenshots/";
}

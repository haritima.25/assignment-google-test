package com.google.helpers.constants;

import java.util.Locale;

public class uiHelpers {

    public static String getCurrentLocation(){
        Locale local = Locale.getDefault();
        return local.getDisplayCountry();
    }
}

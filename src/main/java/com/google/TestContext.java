package com.google;

import org.openqa.selenium.WebDriver;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class TestContext {
    private static final TestContext testContext = new TestContext();
    private WebDriver webDriver;

    public static TestContext getInstance() {
        return testContext;
    }

    public WebDriver getWebDriver() {
        String browserName = System.getProperty("browser") != null ? System.getProperty("browser") : "chrome";
        if (webDriver == null) {
            if (browserName.equalsIgnoreCase("edge"))
                webDriver = DriverSetup.getInstance().getEdgeDriver();
            else if (browserName.equalsIgnoreCase("chrome"))
                webDriver = DriverSetup.getInstance().getChromeDriver();
            else if (browserName.equalsIgnoreCase("firefox"))
                webDriver = DriverSetup.getInstance().getFirefoxDriver();
            else
                try {
                    throw new Exception("Unsupported browser Type");
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
        return webDriver;
    }

    public void closeAll() {
        if (webDriver != null) {
            webDriver.quit();
            webDriver = null;
        }
    }
}

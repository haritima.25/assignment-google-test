package com.google;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import io.github.bonigarcia.wdm.managers.ChromeDriverManager;
import io.github.bonigarcia.wdm.managers.EdgeDriverManager;
import io.github.bonigarcia.wdm.managers.FirefoxDriverManager;
import io.github.bonigarcia.wdm.managers.InternetExplorerDriverManager;
import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class DriverSetup {

    private static final DriverSetup driverSetup = new DriverSetup();

    private DriverSetup() {

    }

    public static DriverSetup getInstance() {
        return driverSetup;
    }

    public WebDriver getChromeDriver() {
        try {
            setupChromeDriverBinary();
            final ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.setAcceptInsecureCerts(true);
            chromeOptions.addArguments(
                "--ignore-certificate-errors",
                "--disable-download-notification",
                "--no-sandbox",
                "--disable-gpu");
            if (SystemUtils.IS_OS_LINUX) {
                chromeOptions.addArguments(
                    "--allow-running-insecure-content",
                    "--disable-web-security",
                    "--disable-dev-shm-usage",
                    "--start-fullscreen");

            } else if (SystemUtils.IS_OS_MAC || SystemUtils.IS_OS_WINDOWS) {
                chromeOptions.addArguments(
                    "--start-fullscreen");
            }

            return new ChromeDriver(ChromeDriverService.createDefaultService(), chromeOptions);
        } catch (UnreachableBrowserException e) {
            throw new UnreachableBrowserException(e.getMessage());
        }

    }

    public WebDriver getEdgeDriver() {
        EdgeOptions edgeOptions=new EdgeOptions();
        edgeOptions.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);

        setupEdgeDriverBinary();
        WebDriver driver = new EdgeDriver(edgeOptions);
        driver.manage().window().maximize();
        return driver;
    }

    public WebDriver getFirefoxDriver() {
        FirefoxOptions firefoxOptions=new FirefoxOptions();
        firefoxOptions.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        firefoxOptions.addArguments("--ignore-certificate-errors",
            "--disable-download-notification");

        setupFirefoxDriverBinary();
        WebDriver driver = new FirefoxDriver(firefoxOptions);
        driver.manage().window().fullscreen();
        return driver;
    }

    private void setupChromeDriverBinary() {
        try {
            if (SystemUtils.IS_OS_MAC || SystemUtils.IS_OS_WINDOWS || SystemUtils.IS_OS_LINUX) {
                ChromeDriverManager.getInstance(DriverManagerType.CHROME).setup();
                WebDriverManager.chromedriver().setup();
            } else {
                System.getenv();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupEdgeDriverBinary() {
        try {
            if (SystemUtils.IS_OS_MAC || SystemUtils.IS_OS_WINDOWS || SystemUtils.IS_OS_LINUX) {
                EdgeDriverManager.getInstance(DriverManagerType.EDGE).setup();
                WebDriverManager.edgedriver().setup();
            } else {
                System.getenv();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupFirefoxDriverBinary() {
        try {
            if (SystemUtils.IS_OS_MAC || SystemUtils.IS_OS_WINDOWS || SystemUtils.IS_OS_LINUX) {
                FirefoxDriverManager.getInstance(DriverManagerType.FIREFOX).setup();
                WebDriverManager.firefoxdriver().setup();
            } else {
                System.getenv();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

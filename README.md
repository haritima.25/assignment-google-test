# Assignment Google Test
This is a BDD based framework designed to run UI automation for google home page and search functionality on chrome, edge and firefox browser

# Framework Design 
1. Java 11
2. Gradle
3. Selenium 3.141.59
4. Cucumber

# Run Regression test Pack 
use below command on terminal. By default it will run using chrome browser 
`gradle regressionTests `
To change the browser type to edge or firefox use command 
`gradle regressionTests -Dbrowser=edge`

# Run single test/ feature file/ test runner on intelliJ 
Right click on the desired test/file > click run
In case what to change the default browser type > select edit configuration on intelliJ and add the variable -Dbrowser = <browserType> in the VM options 
